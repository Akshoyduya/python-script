from scrapy.spiders import BaseSpider
from scrapy.selector import HtmlXPathSelector
from first_scrapy.items import FirstScrapyItem

class MySpider(BaseSpider):
    name = "craig"
    allowed_domains = ["craigslist.org"]
    start_urls = ["http://sfbay.craigslist.org/search/npo"]

    def parse(self, response):
      #  hxs = HtmlXPathSelector(response)
        titles = response.selector.xpath("//p")
        items = []
        for titles in titles:
            item = FirstScrapyItem()
            item["title"] = titles.select("a/text()").extract()
            item["link"] = titles.select("a/@href").extract()
            #print()
            items.append(item)
        return items